package br.com.stream;

public class CharacterStream implements CustomStream{

	private int position = 0;
    private final String value;    

    public CharacterStream(String value) {
        this.value = value;
    }

    public boolean hasNext() {
        return position < value.length();
    }

    public char getNext() {
        return value.charAt(position++);
    }
    
}
