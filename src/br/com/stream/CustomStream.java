package br.com.stream;

public interface CustomStream{
	
	public char getNext();
	public boolean hasNext();
	
}