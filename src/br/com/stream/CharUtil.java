package br.com.stream;

public class CharUtil{

	public static void main(String [] args){
		System.out.println(firstChar("aAbBABacafe"));
	}
	 
	public static char firstChar(String value) {
		CharacterStream input = new CharacterStream(value);
		Character text = null;
		Character firstChar = null;
		boolean vowel = false;
		boolean consonante = false;
		  while(input.hasNext()) {
			 text = input.getNext();	    		
			    if(text== 97 | text== 101 | text== 105 | text== 111 | text== 117){//is a vowel
		        	if(vowel && consonante){	    	        		
		        		if(firstChar != text){//vowel isn't equal the other
		        			firstChar = text;
		        			break;
		        		}
		        		consonante = false;
		        		vowel = false;
		        		continue;
		        	}
		        	vowel = true;
		        	firstChar = text;
		        	continue;
		        }
		        if(text>= 98 && text<= 122 && vowel){//is a consonant and has a vowel before
		        	if(consonante){
		        		consonante = false;
		        		vowel = false;
	    	        	continue;
		        	}
		        	consonante = true;
		        }
		  }   	 
		 return firstChar;
	}
	  
}
